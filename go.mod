module gerrit.wikimedia.org/r/mediawiki/tools/cli

require (
	bou.ke/staticfiles v0.0.0-20210106104248-dd04075d4104
	github.com/Microsoft/go-winio v0.5.0 // indirect
	github.com/ahmetb/govvv v0.3.0
	github.com/alecthomas/assert v0.0.0-20170929043011-405dbfeb8e38
	github.com/alecthomas/colour v0.1.0 // indirect
	github.com/alecthomas/repr v0.0.0-20210801044451-80ca428c5142 // indirect
	github.com/blang/semver v3.5.1+incompatible
	github.com/briandowns/spinner v1.11.1
	github.com/containerd/containerd v1.4.4 // indirect
	github.com/docker/distribution v2.7.1+incompatible // indirect
	github.com/docker/docker v20.10.6+incompatible
	github.com/docker/go-connections v0.4.0 // indirect
	github.com/docker/go-units v0.4.0 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/joho/godotenv v1.3.0
	github.com/manifoldco/promptui v0.7.0
	github.com/moby/term v0.0.0-20201216013528-df9cb8a40635 // indirect
	github.com/morikuni/aec v1.0.0 // indirect
	github.com/opencontainers/go-digest v1.0.0 // indirect
	github.com/opencontainers/image-spec v1.0.1 // indirect
	github.com/rhysd/go-github-selfupdate v1.2.3
	github.com/sergi/go-diff v1.2.0 // indirect
	github.com/spf13/cobra v1.0.0
	github.com/stretchr/testify v1.6.1 // indirect
	github.com/txn2/txeh v1.3.0
	github.com/xanzy/go-gitlab v0.50.4
	golang.org/x/net v0.0.0-20210805182204-aaa1db679c0d // indirect
	golang.org/x/sys v0.0.0-20210921065528-437939a70204 // indirect
	golang.org/x/term v0.0.0-20201126162022-7de9c90e9dd1
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	gotest.tools/v3 v3.0.3 // indirect
)

go 1.13
